import java.io.*;

class SumDemo{

	static int sum=0;
	static int find_sum(int N){

		sum = N*(N+1)/2;
		
		return sum;
	}
	
	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter number :");

		int N = Integer.parseInt(br.readLine());

		int ans = SumDemo.find_sum(N);

		System.out.println(ans);
	}
}


		

