import java.io.*;

class SQRT{

	static int squareroot(int num){

		int start =1;
		int end = num;
		int mid = 1;

		while(start <= end){

			mid = (start+end)/2;

			if(mid*mid <= num){
				if((mid +1)*(mid +1)>num)
					break;

				else
					start = mid +1;
			}
			else{
				end = mid-1;
			}
		}
		return mid;
	}
	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter number :");

		int num = Integer.parseInt(br.readLine());

		int ans = squareroot(num);

		System.out.println(ans);
	}
}


