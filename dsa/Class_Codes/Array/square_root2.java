import java.util.*;

class SQRT{
	static int squareroot(int num){

		int val = 0;
		for(int i=1; i<=num; i++){
			if(i*i>num){
				val = i-1;
				break;
			}
		}
		return val;
	}
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter number :");

		int num = sc.nextInt();

		int ans = squareroot(num);

		System.out.println(ans);
	}
}

