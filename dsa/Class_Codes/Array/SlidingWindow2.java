class Demo{

	public static void main(String[] args){

		int k=4;
		int end = k-1;

		int arr[] = new int[]{-3,4,-2,5,3,-2,8,2,1,4};
		int N = arr.length;
		int sum=0;

		for(int i=0; i<=end; i++){

			sum += arr[i];
		}

		int start = 1; 
		end = k;
		int max = sum;

		while(end < N){

			sum = sum - arr[start-1] + arr[end];

			if(sum>max)
				max = sum;

			start++;
			end++;
		}
		System.out.println(max);
	}
}

