import java.io.*;

class DiffDemo{

	static int start=0;
	static int end=0;
	static int diff(int start, int end){

		int ans =0;
		ans = (end-start)+1;
		return ans;
	}
	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter starting number :");
		int start = Integer.parseInt(br.readLine());

		System.out.println("Enter ending number :");
		int end = Integer.parseInt(br.readLine());

		int anss = diff(start,end);

		System.out.println(anss);
	}
}



