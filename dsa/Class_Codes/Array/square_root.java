import java.io.*;

class SQRT{

	static int squareroot(int num){

		int count =0;

		for(int i=1; i*i<=num; i++){

			count++;
		}
		return count ;
	}
	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter number :");

		int num = Integer.parseInt(br.readLine());

		int ans = squareroot(num);

		System.out.println(ans);
	}
}
