
//find shortest sub-array that contain max and min element of an array

import java.util.*;

class ShortLengthArray{
        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);
		System.out.println("Enter an Array size :");
                int size = sc.nextInt();

                int arr[] = new int[size];

                System.out.println("Enter Array elements :");
                for(int i = 0;i<arr.length; i++){

                        arr[i] = sc.nextInt();
                }

		int maxEle = Integer.MIN_VALUE;
		int minEle = Integer.MAX_VALUE;

		for(int i=0; i<size; i++){

			if(arr[i]>maxEle)
				maxEle = arr[i];
			if(arr[i]<minEle)
				minEle = arr[i];
		}
		int len = 0;
		int minLength = Integer.MAX_VALUE;

		for(int i=0; i<size; i++){

			if(arr[i] == maxEle){

				for(int j=i+1; j<size; j++){

					if(arr[j] == minEle){
						len = j-i+1;

						if(len<minLength)
							minLength = len;
					}
				}
			}else if(arr[i] == minEle){

                                for(int j=i+1; j<size; j++){

                                        if(arr[j] == maxEle){
                                                len = j-i+1;

                                                if(len<minLength)
                                                        minLength = len;
                                        }
                                }
                        }
		}
		System.out.println(minLength);
		
	}
}



