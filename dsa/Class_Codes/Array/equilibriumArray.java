import java.util.*;

class EquilibriumArrayDemo{
        public static void main(String[] args){

 		Scanner sc = new Scanner(System.in);
 
		System.out.println("Enter Array size :");
 		int size = sc.nextInt();

                int arr[] = new int[size];

                System.out.println("Enter Array elements :");
                for(int i = 0;i<arr.length; i++){

                        arr[i] = sc.nextInt();
                }

		int LM[] = new int[size];
		LM[0] = arr[0];
		
		for(int i=1 ; i<size; i++){

			LM[i] = LM[i-1] + arr[i];
		}

		int RM[] = new int[size];                // ===========>  Optimized Approach
							 // TC = O(N)	
							 // SC = O(N)
		RM[size-1] = arr[size-1];

		for(int i = (size-2); i>=0; i--){

			RM[i] = RM[i+1] + arr[i];
		}

		int count = 0;
		for(int i=0; i<size; i++){

			if(i==0){
				if(RM[i+1] == 0)
					count++;
			}else if(i==(size-1)){
				if(LM[i-1]==0)
					count++;
			}
			else{
				if(RM[i+1] == LM[i-1])
					count++;
			}
		}

		System.out.println(count);
	}
}



