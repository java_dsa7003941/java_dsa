class ArrayDemo{
	public static void main(String[] args){

		int arr[] = new int[]{2,5,3,6,3,8,5,7};

		int maxEle1 = Integer.MIN_VALUE;
		int maxEle2 = Integer.MIN_VALUE;

		for(int i=0; i<arr.length; i++){
			if(arr[i]>maxEle1)
				maxEle1 = arr[i];
		}
		for(int i=0; i<arr.length; i++){
			if(arr[i]>maxEle2 && arr[i]<maxEle1)
				maxEle2  = arr[i];
		}
		System.out.println(maxEle2);
	}
}

