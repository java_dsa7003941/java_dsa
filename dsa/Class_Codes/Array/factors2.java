import java.io.*;

class Factors{

	static int find_factors(int num){

		int count = 3;

		for(int i=2; i<num/2; i++){
			if(num%i==0)
				count++;
		}
		return count;
	}
	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter number :");
		int num = Integer.parseInt(br.readLine());

		int ans = find_factors(num);

		System.out.println(ans);
	}
}


