import java.util.*;

class FindPairs{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		int size = sc.nextInt();

		char arr[] = new char[size];

		System.out.println("Enter character :");
		for(int i = 0;i<arr.length; i++){
			
			arr[i] = sc.next().charAt(0);
		}

		int count = 0;
		int pair = 0; 

		for(int i=0; i<arr.length; i++){

			if(arr[i] == 'a'){
				count++;
			}else if(arr[i] == 'g'){
				pair = pair + count;
			}

		}
		System.out.println(pair);
	}
}

