class ArrayDemo{
	public static void main(String[] args){

		int arr[] = new int[]{3,5,7,4,8,5,9,4,6,9,2};

		int maxEle = Integer.MIN_VALUE;

		for(int i=0; i<arr.length; i++){
			if(arr[i]>maxEle)
				maxEle = arr[i];
		}
		int count = 0;
		for(int i=0; i<arr.length;i++){
			if(maxEle != arr[i])
				count++;
		}
		System.out.println(count);
	}
}

