import java.io.*;

class BinarySrch{

	static int binary(int arr[], int search){

		int start=0, end = arr.length-1;

		while(start<=end){

			int mid = (start + end)/2;

			if(arr[mid] == search)
				return mid;

			if(arr[mid]>search)
				end = mid-1;

			if(arr[mid]<search)
				start = mid +1;

		}
		
		return -1;	
				
	}
	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int arr[] = new int[]{7,13,23,43,76,84,97,99};

		int search = Integer.parseInt(br.readLine());

		int index = binary(arr,search);

		if(index == -1)
			System.out.println("Element not found");
		else
			System.out.println("Element found at index : " + index);

	}
}

