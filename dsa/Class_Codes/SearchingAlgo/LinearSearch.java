import java.util.*;

class Linear{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		int arr[] = new int[]{3,2,6,1,9,8};

		int search = sc.nextInt();

		int index = linearSearch(arr,search);

		if(index==-1)
			System.out.println("Element not found");
		else
			System.out.println("Element found at index : " + index);	

	}

	static int linearSearch(int arr[],int search){

		for(int i=0; i<arr.length; i++){

			if(arr[i]==search){
				return i;
					
			}
		}
		return -1;
	}
}
