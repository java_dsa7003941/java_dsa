import java.io.*;

class BinarySrch{

	static int binary(int arr[],int start,int end, int search){

		if(start>end)
			return -1;

			int mid = (start + end)/2;

			if(arr[mid] == search)
				return mid;

			if(arr[mid]>search)
				return binary(arr,start,mid-1,search);

			else
				return binary(arr,mid+1,end,search);	
				
	}
	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int arr[] = new int[]{7,13,23,43,76,84,97,99};

		int search = Integer.parseInt(br.readLine());

		int start =0,end = arr.length-1;
		int index = binary(arr,start,end,search);

		if(index == -1)
			System.out.println("Element not found");
		else
			System.out.println("Element found at index : " + index);

	}
}

