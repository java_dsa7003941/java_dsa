class QuickS{

	int partition(int arr[], int start, int end){

		int pivot = arr[end];
		int i = start-1;

		for(int j=0; j<end; j++){

			if(arr[j]<pivot){
				i++;

				int temp = arr[j];
				arr[j] = arr[i];
				arr[i] = temp;
			}
		}

		int temp = arr[i+1];
		arr[i+1] = arr[end];
		arr[end] = temp;

		return i+1;
	}

	void quicksort(int arr[], int start,int end){

		if(start < end){

			int pivotIdx = partition(arr,start,end);

			quicksort(arr,start,pivotIdx-1);
			quicksort(arr,pivotIdx+1,end);
		}
	}

	public static void main(String[] args){

		QuickS obj = new QuickS();

		int arr[] = new int[]{5,2,4,7,9,1,8};

		obj.quicksort(arr,0,arr.length-1);

	}
}
