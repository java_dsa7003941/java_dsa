class BubbleS{

	public static void main(String[] args){

		int arr[] = new int[]{4,1,3,7,5,2,8,9};

		sort(arr);
	}
	static void sort(int arr[]){

		int count=0;
		for(int i=0; i<arr.length; i++){

			for(int j=0; j<arr.length-i-1; j++){

				count++;
				if(arr[j]>arr[j+1]){

					int temp = arr[j];
					arr[j] = arr[j+1];
					arr[j+1] = temp;
				}
			}
		}

		for(int i=0; i<arr.length; i++){

			System.out.print(arr[i] + " ");
		}

		System.out.println();
		System.out.println(count);
	}
}


