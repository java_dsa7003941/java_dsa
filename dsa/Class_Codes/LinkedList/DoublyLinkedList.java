class Node{

	Node prev=null;
	Node next=null;
	int data;

	Node(int data){
		this.data=data;
	}
}

class DoublyLinkedList{

	Node head= null;

	void addFirst(int data){

		Node newNode = new Node(data);

		if(head==null){
			head=newNode;
		}
		else{
			newNode=head;
			head.prev=newNode;
			head=newNode;
		}
	}

	void printDLL(){

		if(head==null){
			System.out.println("Empty List");
		}
		else{
			Node temp=head;

			while(temp!=null){

				System.out.println(temp.data + " " );
				temp = temp.next;

			}
		}
	}
}

class Client{

	public static void main(String[] args){

		DoublyLinkedList dll = new DoublyLinkedList();

		dll.addFirst(10);
		dll.addFirst(20);

		dll.printDLL();
	}
}

			
