class SumOfDigits{

	static int fun(int num){

		if(num==0)
			return 0;

		return num%10 + fun(num/10);

	}

	public static void main(String[] args){

		int ret = fun(54321);

		System.out.println(ret);
	}
}
