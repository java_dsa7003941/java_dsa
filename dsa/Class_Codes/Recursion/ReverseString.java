class ReverseStr{

	public static void main(String[] args){

		String str = "core2web";

		ReverseStr obj = new ReverseStr();

		String str2 = obj.rev(str);

		System.out.println(str2);
	}

	/*String rev(String str){

		String str1 ="";

		for(int i=str.length()-1; i>=0; i--){         ======--> Using Simple method

			str1 += str.charAt(i);
		}

		return str1;
	}*/

	String rev(String str){

		if(str == null || str.length()<=1)
			return str;

		return rev(str.substring(1)) + str.charAt(0);
	}

}



