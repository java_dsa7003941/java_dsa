class FactorialNum{

	public static void main(String[] args){

		FactorialNum obj = new FactorialNum();

		int ret = obj.fact(5);

		System.out.println(ret);
	}

	int fact(int num){

		if(num==0)
			return 1;

		return num*fact(num-1);
	}
}

