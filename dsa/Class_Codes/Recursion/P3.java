class SumOfN{

	int fun(int num){

		if(num==1)
			return 1;

		return num + fun(--num);
	}

	public static void main(String[] args){

		SumOfN obj = new SumOfN();
		int ret = obj.fun(10);

		System.out.println(ret);
	}
}

