class Demo{

	public static void main(String[] args){

		int num = 10;

		fun(num);
	}

	static void fun(int num){

		if(num==0)
			return;

		System.out.println(num);
		num--;

		fun(num);

	}
}


