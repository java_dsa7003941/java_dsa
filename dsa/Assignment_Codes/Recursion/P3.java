class Demo{

	public static void main(String[] args){

		int num = 2467;

		int ans = fun(num);

		System.out.println(ans);
	}

	static int sum=0;
	static int fun(int num){

		if(num==0)
			return sum;

		sum = num%10 + sum;

		fun(num/10);

		return sum;
	}
}

