import java.util.*;

class ArrayDemo{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Array size :");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Array elements :");
		for(int i=0; i<arr.length; i++){

			arr[i] = sc.nextInt();
		}

		int maxEle = Integer.MIN_VALUE;
		int minEle = Integer.MAX_VALUE;

		for(int i=0; i<arr.length; i++){
			if(arr[i]>maxEle)
				maxEle = arr[i];
			if(arr[i]<minEle)
				minEle = arr[i];
		}
		System.out.println(maxEle + minEle);
	}
}

