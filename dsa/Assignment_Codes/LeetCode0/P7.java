import java.util.*;

class ArrayDemo{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		int arr[] = new int[]{2,4,1,3,2};

		int maxEle = Integer.MIN_VALUE;

		for(int i=0; i<arr.length; i++){
			if(arr[i]>maxEle)
				maxEle = arr[i];

		}
		int count = 0;
		for(int i=0; i<arr.length; i++){

			int diff = 0;
			if(arr[i]<maxEle){
				diff = maxEle-arr[i];
			}
			for(int j=0; j<diff; j++){
				count++;
			}
		}
		System.out.println(count);
	}
}




