import java.util.*;

class ArrayDemo{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Array size :");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter an Array Elements :");
		int prod =1;
		for(int i =0; i<size; i++){
			arr[i] = sc.nextInt();
			prod = prod*arr[i];
		}
		System.out.println("==================");
		for(int i=0; i<size; i++){
			arr[i] = prod/arr[i];
			System.out.println(arr[i]);
		}
	}
}
