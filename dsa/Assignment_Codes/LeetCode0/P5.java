import java.util.*;

class ArrayDemo{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Array size :");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Array element :");
		for(int i=0; i<arr.length; i++){
			arr[i] = sc.nextInt();
		}

		System.out.println("Enter number to search :");
		int B = sc.nextInt();

		int count =0;
		for(int i=0; i<size ; i++){
			if(arr[i]==B)
				count++;
		}
		System.out.println(count);
	}
}

		

