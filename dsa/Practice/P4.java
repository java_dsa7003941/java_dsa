import java.io.*;

class Demo{

	public static void main(String []args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size :");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements ");
		for(int i=0; i<arr.length; i++){

			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Enter elements to search ");
		int search = Integer.parseInt(br.readLine());

		int val = binarySearch(arr,search);

		if(val==-1)
			System.out.println("Element not found");
		else
			System.out.println("Element found at "+ val +" index");
	}
	static int binarySearch(int arr[], int search){

		int start=0, end = arr.length-1;

		while(start<=end){
			int mid = start + ((end-start)/2);

			if(arr[mid] == search)
				return mid;

			if(arr[mid]<search)
				start=mid+1;;
			if(arr[mid]>search)
				end=mid-1;
		}
		return -1;
	}
}

