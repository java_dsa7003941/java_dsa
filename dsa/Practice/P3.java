import java.util.*;

class Demo{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		Demo obj = new Demo();

		System.out.println("Enter Array size ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Array Elements ");
		for(int i=0; i<arr.length; i++){

			arr[i] = sc.nextInt();
		}

		obj.bubbleSort(arr);

		for(int i=0; i<arr.length; i++){

			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}

	void bubbleSort(int arr[]){

		for(int i=0; i<arr.length; i++){

			boolean flag = false;

			for(int j=0; j<arr.length-1-i; j++){

				if(arr[j]>arr[j+1]){

					int temp = arr[j];
					arr[j] = arr[j+1];
					arr[j+1] = temp;
					flag = true;
				}
			}
			if(flag == false)
				break;
		}
	}
}

