import java.io.*;
import java.util.*;

class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Array size ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter Array Elements ");

		for(int i=0; i<arr.length; i++){

			arr[i] = Integer.parseInt(br.readLine());
		}

		int start=0,end = arr.length-1;

		Demo obj = new Demo();

		obj.sort(arr,start,end);

		for(int i=0; i<arr.length; i++){

			System.out.print(arr[i] + " ");
		}

		System.out.println();
	}

	void sort(int arr[], int start, int end){

		if(start<end){

			int mid = start +((end-start)/2);

			sort(arr,start,mid);
			sort(arr,mid+1,end);

			mergeSort(arr,start,mid,end);
		}
	}

	void mergeSort(int arr[] ,int start,int mid,int end){

		int n1 = mid-start+1;
		int n2 = end-mid;

		int arr1[] = new int[n1];
		int arr2[] = new int[n2];

		for(int i=0; i<arr1.length; i++){

			arr1[i] = arr[start+i];
		}

		for(int i=0; i<arr2.length; i++){

			arr2[i] = arr[mid+1+i];
		}

		int i=0,j=0,k=start;

		while(i<arr1.length && j<arr2.length){

			if(arr1[i]<arr2[j]){

				arr[k] = arr1[i];
				i++;
			}else{
				arr[k] = arr2[j];
				j++;
			}
			k++;
		}

		while(i<arr1.length){

			arr[k] = arr1[i];
			i++;
			k++;
		}
		while(j<arr2.length){

			arr[k] = arr2[j];
			j++;
			k++;
		}
	}
}
			

