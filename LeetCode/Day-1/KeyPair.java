import java.io.*;

class Keypair{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size :");
		int size = Integer.parseInt(br.readLine());
		int arr[] = new int[size];

		System.out.println("Enter array elelments ");
		for(int i=0; i<arr.length; i++){

			arr[i] = Integer.parseInt(br.readLine());
		}
		System.out.println("Enter number of addition :");
		int num = Integer.parseInt(br.readLine());

		int count=0;
		for(int i=0; i<arr.length; i++){

			for(int j=i+1; j<arr.length; j++){

				if((arr[i] + arr[j])==num){
					System.out.println("Yes " + arr[i] + " & " + arr[j]);
					count++;
				}
			}
		}
		if(count==0)
			System.out.println("No");
	}
}

