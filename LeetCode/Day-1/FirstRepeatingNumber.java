import java.util.*;

class FirstRepeatingNo{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		int size = sc.nextInt();

		int arr[] = new int[size];

		for(int i=0; i<arr.length; i++){

			arr[i] = sc.nextInt();
		}

		int count=0;
		for(int i=0; i<arr.length; i++){

			for(int j=i+1; j<arr.length; j++){

				if(arr[i] == arr[j]){
					System.out.println(i+1);
					count++;
					break;
				}
			}
			if(count==1)
				break;
		}
		if(count==0)
			System.out.println("-1");
	}
}


