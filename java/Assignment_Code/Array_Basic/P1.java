import java.io.*;

class Demo{
	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size of an Array :");
		int size = Integer.parseInt(br.readLine());

		System.out.println("Enter elements in an Array :");
		int arr[] = new int[size];

		for(int i=0; i<arr.length; i++){

			arr[i] = Integer.parseInt(br.readLine());
			
		}
		System.out.println("Enter element to search :");
		int no = Integer.parseInt(br.readLine());
		for(int i=0; i<arr.length; i++){

			if(no==arr[i]){
				System.out.println("Index of " + arr[i] + " is " + i);
			}
		}
	}
}


