import java.io.*;

class Demo{
        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter size of an Array :");
                int size = Integer.parseInt(br.readLine());

                System.out.println("Enter elements in an Array :");
                int arr[] = new int[size];

                for(int i=0; i<arr.length; i++){

                        arr[i] = Integer.parseInt(br.readLine());

                }
                int max=arr[0];
		for(int i=0; i<arr.length; i++){
			if(arr[i]>max){
				max = arr[i];
			}
		}
		System.out.println("Maximum element from an Array is : " + max);
	}
}

