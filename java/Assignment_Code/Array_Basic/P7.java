import java.io.*;

class form_largest_number {
        static int form_largest(int arr[]){
                int Output = 0;
                int mul = 1;
                int min_index = 0;

                for(int i = 0 ; i < arr.length ; i++){

                        min_index = i;

                        for(int j = i + 1 ; j < arr.length ; j++)
                                if(arr[min_index] > arr[j])
                                        min_index = j;


                        int temp = arr[min_index];
                        arr[min_index] = arr[i];
                        arr[i] = temp;

                        Output = Output + (arr[i] * mul);
                        mul = mul * 10;
                }

                return Output;
        }

        public static void main(String[] args)throws IOException {
                
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter an Array size :");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter an Array elements :");
		for(int i=0; i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

                System.out.println(form_largest(arr));
        }
}



