class Demo {
    public static boolean checkElementsInRange(int[] arr, int A, int B) {
        if (A > B) {
            return false; // invalid range
        }
 
        for (int i = A; i <= B; i++) {
            boolean found = false;
            for (int j = 0; j < arr.length; j++) {
                if (arr[j] == i) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                return false; // element not found in array
            }
        }
        return true; // all elements in range found in array
    }
 
    public static void main(String[] args) {
        int[] arr = {1, 4, 5, 2, 7, 8, 3};
        int A = 2, B = 6;
        if (checkElementsInRange(arr, A, B)) {
            System.out.println("Yes");
        } else {
            System.out.println("No");
        }
    }
}
			
