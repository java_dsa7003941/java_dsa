// Program for finding max. element from an Array.

import java.util.*;

class ArrayMax{
        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);

                System.out.println("Enter Array size :");
                int size = sc.nextInt();
                int arr[] = new int[size];

                System.out.println("Enter Array Elements :");
                for(int i =0; i<arr.length ; i++){
                        arr[i] = sc.nextInt();
                }

                int max = arr[0];
                for(int i=0; i<arr.length; i++){

                        if(max<arr[i]){
                                max = arr[i];
                        }
                }
                System.out.println("Max. Element is :" + max);
        }
}
