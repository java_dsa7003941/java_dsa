// Program for finding min. element from an Array.

import java.util.*;

class ArrayDemo{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter Array size :");
		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.println("Enter Array Elements :");
		for(int i =0; i<arr.length ; i++){
			arr[i] = sc.nextInt();
		}

		int min = arr[0];
		for(int i=0; i<arr.length; i++){

			if(min>arr[i]){
				min = arr[i];
			}
		}
		System.out.println("Min. Element is :" + min);
	}
}



