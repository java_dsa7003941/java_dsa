// Program for if one thread close the pipe after his work then keyboard can close the connection with jvm.
// means that no other thread presnt in that jvm are unable to take input from keyboard
 

import java.io.*;

class IODemo{
	public static void main(String[] args)throws IOException{

		BufferedReader br1 = new BufferedReader(new InputStreamReader(System.in));
		BufferedReader br2 = new BufferedReader(new InputStreamReader(System.in));

				String str1 = br1.readLine();
				System.out.println("String1 =" + str1);
				br1.close();

				String str2 = br2.readLine();
				System.out.println("String2 =" + str2);

	}
	}
