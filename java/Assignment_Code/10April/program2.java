// StringTokenizer Program

import java.io.*;
import java.util.*;

class DemoStringTokenizer{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter match info, MOM, Runs");
		String info = br.readLine();

		StringTokenizer st = new StringTokenizer(info, " ");

		String token1 = st.nextToken();
		String token2 = st.nextToken();
		String token3 = st.nextToken();

		System.out.println("Match info =" + token1);
		System.out.println("MOM =" + token2);
		System.out.println("Runs =" + token3);

	}
}

