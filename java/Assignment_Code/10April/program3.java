import java.io.*;
import java.util.*;

class DemoStringTokenizer{

        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter match info, Enter Group, Total Matches, Live viewer in Cr. ");
                String info = br.readLine();

                StringTokenizer st = new StringTokenizer(info, ",");

                String token1 = st.nextToken();
                char token2 = st.charAt(0);
                int token3 = Integer.parseInt(st.nextToken());
		float token4 = Float.parseFloat(st.nextToken());

                System.out.println("Match info =" + token1);
                System.out.println("Group =" + token2);
                System.out.println("Total Matches =" + token3);
		System.out.println("Live viewers in Cr. =" + token4);

        }
}
