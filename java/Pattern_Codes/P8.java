class Demo{

	public static void main(String[] args){

		int row =5;
		int star=row*2-1;
		int space=0;

		for(int i=1; i<row*2; i++){

			for(int j=1; j<=space; j++){

				System.out.print("  ");
			}

			for(int k=1; k<=star; k++){

				System.out.print("* ");
			}

			for(int m=1; m<=space; m++){

				System.out.print("  ");
			}
			
			if(i<=row){
				star-=2;
				space++;
			}else{
				star+=2;
				space--;
			}

			System.out.println();
		}
	}
}

