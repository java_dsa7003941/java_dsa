class ButterflyPattern {
    public static void main(String[] args) {

        int rows = 5;
	int star = 1;
	int space = (rows*2)-2;

	for(int i=1; i<=(rows*2)-1; i++){

		for(int j = 1; j<=star; j++){

			System.out.print("* ");
		}
			for(int sp=1; sp<=space; sp++){

				System.out.print("  ");
			}
				for(int k=1; k<=star; k++){

					System.out.print("* ");

				}
					if(i<rows){
						star++;
						space -= 2;
					}else{
						star--;
						space += 2;
					
					}
					System.out.println();
	}	

    }
}
