class Demo{
	public static void main(String[] args){

		int rows = 3;
		int star=1;
		int space = rows-1;

		for(int i=1; i<=(rows*2)-1; i++){

			for(int j=1;j<=space;j++){
				
				System.out.print("  ");
			}
			for(int k=1; k<=star; k++){
				System.out.print("* ");
			}
			if(i<rows){
				space--;
				star++;
			}else{
				space++;
				star--;
			}
			System.out.println();
		}
	}
}


