import java.io.*;

class DataOverflowException extends RuntimeException{

	DataOverflowException(String str){
		super(str);
	}
}

class DataUnderflowException extends RuntimeException{

        DataUnderflowException(String str){
                super(str);
        }
}

class ExceptionDemo{
	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Integer Data ");
		System.out.println("0 < Data < 100");

		int arr[] = new int[4];

		for(int i=0; i<arr.length; i++){
			
			int data = Integer.parseInt(br.readLine());

			if(data < 0){
				throw new DataUnderflowException("Data less than 0 ");
			}if(data > 100){
				throw new DataOverflowException("Data greater than 0 ");
			} 

				arr[i] = data;
		}

		for(int i=0; i<arr.length; i++){
			System.out.print(arr[i] + " ");
		}
	}
}

		
		
