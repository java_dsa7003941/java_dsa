class Demo{

	public static void main(String[] args){

		int arr[] = new int[]{-2,1,-3,4,-1,2,1,-5,4};

		int startIdx=-1,endIdx=-1,x=-1;
		int sum=0;
		int maxSum = Integer.MIN_VALUE;

		for(int i=0; i<arr.length;i++){

			if(sum==0)
				x=i;

			sum = arr[i] + sum;

			if(sum>maxSum){

				maxSum = sum;

				startIdx = x;
				endIdx = i;
			}

			if(sum<0)
				sum=0;

		}

		for(int i=startIdx; i<=endIdx; i++){

			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}
}

