class Demo{

	static int firstNonRepeatingChar(String str){

		for(int i=0; i<str.length(); i++){

			boolean flag = true;
			char ch= str.charAt(i);

			for(int j=i+1; j<str.length(); j++){

				if(ch == str.charAt(j))
					flag = false;

			}
			if(flag == true)
				return i;
		}
		return -1;
	}

	public static void main(String[] args){

		int a = firstNonRepeatingChar("aaaa");

		System.out.println(a);
	}
}

