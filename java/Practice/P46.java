class Demo{

	static int searchFun(int arr[], int search,int start,int end){

		if(start>end)
			return -1;

		else{
			int mid = start + ((end-start)/2);

			if(arr[mid]==search)
				return mid;

			if(arr[mid]>search)
				return searchFun(arr,search,start,mid-1);

			else
				return searchFun(arr,search,mid+1,end);
		}
	}

	public static void main(String[] args){

		int arr[] = new int[]{12,23,34,45,56,67,78,89};
		int search = 66;

		int ret = searchFun(arr,search,0,arr.length-1);

		if(ret==-1)
			System.out.println("Element not found");
		else
			System.out.println("Element found at index : " + ret);
	}
}
