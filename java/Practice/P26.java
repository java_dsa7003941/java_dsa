class Demo{
	public static void main(String[] args){

		int arr1[] = {1,3,5,7};
		int arr2[] = {2,4,6,8};

		int length = arr1.length + arr2.length;

		int arr3[] = new int[length];

		int i=0;
		int j=0;
		int k=0;

  			while(i!=arr1.length && j!=arr2.length){

			if(arr1[i]<arr2[j]){
				arr3[k] = arr1[i];
				i++;
				k++;
			}else{
				arr3[k] = arr2[j];
				j++;
				k++;
			}

		}
		
		for( k=0; k<arr3.length; k++){
			System.out.print(arr3[k]+ ", " );
			
		}
		System.out.println();

	}
}

