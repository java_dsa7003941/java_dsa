class MyThread extends Thread{
	MyThread(String str){

		super(str);

	}
	public void run(){

		Thread t = Thread.currentThread();
		System.out.println(Thread.currentThread().getName());
		System.out.println(t.getPriority());
	}
}

class Client{
	public static void main(String[] args){

		MyThread obj1 = new MyThread("A");
		//obj1.setName("Thread-A");
		obj1.start();

		MyThread obj2 = new MyThread("B");
		//obj2.setName("Thread-B");
		obj2.start();
	}
}
