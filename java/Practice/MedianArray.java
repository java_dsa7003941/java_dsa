class Solution {
    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
        
        int nums3[] = new int[nums1.length + nums2.length];

        int i=0,j=0,k=0;

        while(i<nums1.length && j<nums2.length){

            if(nums1[i]>nums2[j]){
                nums3[k] = nums2[j];
                j++;
            }
            else{
                nums3[k] = nums1[i];
                i++;
            }
            k++;
        }
        while(i<nums1.length){
            nums3[k] = nums1[i];
            i++;
            k++;
        }
        while(j<nums2.length){
            nums3[k] = nums2[j];
            j++;
            k++;
        }
        int m=0,n=nums3.length-1;
        while(m<n || m==n){

	    m++;
	    n--;
        }

        double finaal ;
	if(m==n){
        	finaal = (nums3[m]);
	}
	else{
		finaal = (nums3[m] + nums3[n])/2;
	}

        return finaal;
    }
    public static void main(String[] args){

        Solution obj = new Solution();

        int arr1[] = new int[]{1,2};
        int arr2[] = new int[]{3,4};

        double ans = obj.findMedianSortedArrays(arr1,arr2);
        System.out.println(ans);
    }
}
