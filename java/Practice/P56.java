import java.io.*;

class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter String u want to reverse :");
		String str1 = br.readLine();
		String str2 = " ";

		/*for(int i=0; i<str1.length();i++){

			char ch = str1.charAt(i);       =============Way-1
			str2 = ch + str2;
		}

		System.out.println("Your reverse string is : " + str2);
	        */

		char ch[] = str1.toCharArray();

		for(int i=0; i<ch.length; i++){

			str2 = ch[i] + str2;          //============== Way-2
		}
		System.out.println(str2);

	}
}


