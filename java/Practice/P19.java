class MyThread implements Runnable{
	
	public void run(){
		System.out.println(Thread.currentThread().getName());
	}
}

class Client{
	public static void main(String[] args){

		MyThread obj1 = new MyThread();
		Thread t1 = new Thread(obj1);
		t1.start();

		MyThread obj2 = new MyThread();
		Thread t2 = new Thread(obj2);
		t2.start();

	}
}

