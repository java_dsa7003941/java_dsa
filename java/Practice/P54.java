class SingleTon{

	static SingleTon obj = new SingleTon();

	private SingleTon(){

		System.out.println("Private Constructor");
	}

	static SingleTon getObject(){

		return obj;
	}
}
class Demo{

	public static void main(String[] args){

		SingleTon obj1 = SingleTon.getObject();
		SingleTon obj2 = SingleTon.getObject();

		System.out.println(obj1.hashCode());
		System.out.println(obj2.hashCode());
	}
}

