import java.io.*;

class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		String str1 = br.readLine();
		System.out.println(str1);

		int data=0;
		try{
			data = Integer.parseInt(br.readLine());
		
		}catch(NumberFormatException nfe){
			System.out.println("Enter Integer Data");
			data = Integer.parseInt(br.readLine());
		}
		System.out.println(data);
	}
}

