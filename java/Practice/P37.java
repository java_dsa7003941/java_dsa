class Demo{

	public static void main(String[] args){

		int arr[] = new int[]{2,3,4,6,8,9,10,24,67};

		int search = 10;

		int ans = fun(arr,search);

		if(ans == -1)
			System.out.println("Element not found");
		else
			System.out.println("Element found at index : " + ans);

	}

	static int fun(int arr[], int search){

		int start = 0;
		int end = arr.length-1;

		while(start<end){

			int mid = (start + end)/2;

			if(arr[mid] == search)
				return mid;

			if(arr[mid]>search)
				end = mid-1;

			if(arr[mid]<search)
				start = mid+1;

		}
		return -1;
	}
}
