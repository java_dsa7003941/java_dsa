class Demo{

	public static void main(String[] args){

		int num = 145;

		int ans = fun(num);
		System.out.println(ans);
	}

	static int fun(int num){

		int sum=0;
		for(int i=0; num>0; num=num/10){

			int rem = num%10;

			int fact=1;
			for(int j=1; j<=rem; j++){

				fact = fact*j;
			}

			sum = sum +fact;
		}

		return sum;
	}
}
