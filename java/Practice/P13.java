class Demo{
	public static void main(String[] args){

		SingleTon obj = SingleTon.getObject();
		System.out.println(obj);

		SingleTon obj1 = SingleTon.getObject();
                System.out.println(obj1);
	}
}

		

class SingleTon{
	private SingleTon(){

		System.out.println("Constructor");
	}

		static SingleTon obj = new SingleTon();
		
		static SingleTon getObject(){
			return obj;
		}
}

