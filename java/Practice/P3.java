class icc{

	int ranking = 1;
	String cricketBoardName = "BCCI";
	static String iccRules = "One day match must be of 50 overs";

	void match(){

		System.out.println("The Rules of ICC is " + iccRules);
		System.out.println(cricketBoardName + " is comes under ICC");
		System.out.println("Ranking of " + cricketBoardName + " is " + ranking);
	}
}

class mainDemo{

	public static void main(String[] args){

		icc obj1 = new icc();
		icc obj2 = new icc();

		obj1.match();
		obj2.match();

		System.out.println("---After Change in obj2---");

		obj2.ranking = 8;
		obj2.cricketBoardName = "PCB";
		obj2.iccRules = "In 50 overs game, 1 Bowler having only 10 overs to delivered";

		obj1.match();
		obj2.match();
	}
}



