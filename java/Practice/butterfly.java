class ButterflyDemo{
	public static void main(String[] args){

		int row =5;
		int star =1;
		int space = (row*2)-2;

		for(int i=1; i<=(row*2); i++){
			for(int j=1; j<=star; j++){
				System.out.print("* ");
			}
			for(int k=1; k<=space; k++){
				System.out.print("  ");
			}
			for(int m=1;m<=star; m++){
			       System.out.print("* ");	
				
			}
			if(i<row){
				star++;
				space -=2;
			}else{
				star--;
				space +=2;
			}

			System.out.println();
		}
	}
}



