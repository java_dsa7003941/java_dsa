import java.util.*;

class Demo{

	static void sort(int arr[]){

		boolean flag ;

		for(int i=0; i<arr.length-1; i++){

			flag = false;

			for(int j=0; j<arr.length-1-i; j++){
				if(arr[j]>arr[j+1]){

					int temp = arr[j];
					arr[j] = arr[j+1];
					arr[j+1] = temp;
					flag = true;
				}
			}
			if(flag==false)
				break;
			
		}
	}

				

	public static void main(String[] args){

		int arr[] = new int[]{32,37,12,87,98,44,21};

		sort(arr);

		System.out.println(Arrays.toString(arr));
	}
}
