import java.util.*;

class Demo{

	static void sort(int arr[] , int N){

		if(N==1)
			return;

		boolean flag = false;

		for(int j=0; j<arr.length-1-j; j++){

			if(arr[j]>arr[j+1]){

				int temp = arr[j];
				arr[j] = arr[j+1];
				arr[j+1] = temp;
				flag = true;
			}
		
			if(flag == false)
				break;

		}

		sort(arr,N-1);
	}

	public static void main(String[] args){

		int arr[] = new int[]{44,25,28,95,46,81};

		sort(arr,arr.length);

		System.out.println(Arrays.toString(arr));
	}
}
