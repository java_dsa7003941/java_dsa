class Hospital{

	static String name = "JJ Hospital";
	int patientID = 10;
	static int floors = 4;

	void ward(){

		System.out.println(name + " has " + floors + " floors");
		System.out.println("patientID no. " + patientID + " is recently admitted to hospital");
	}
	static void reception(){

		System.out.println(name + " is very reputed hospital");
	}
}

class Patient{

	public static void main(String[] args){

		Hospital obj = new Hospital();
		 
		obj.ward();
		obj.reception();

		System.out.println(obj.name);
		System.out.println(obj.patientID);
		System.out.println(obj.floors);
	}
}

