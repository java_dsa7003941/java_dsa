class Demo{

	public static void main(String[] args){

		int num = 1456;
		int ans = fun(num);
		if(ans==num)
			System.out.println("Palindrome No.");
		else
			System.out.println("Not Palindrome No.");	

	}

	static int fun(int num){

		int sum=0;
		for(int i=0; num>0; num= num/10){

			sum = num%10 + sum*10;
		}

		return sum;
	}
}
