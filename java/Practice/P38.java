class Demo{

	public static void main(String[] args){

		int arr[] = new int[]{3,1,6,4,43,89,31};

		fun(arr);
	       	
	}

	static void fun(int arr[]){

		for(int i=0; i<arr.length; i++){

			for(int j=1; j<arr.length; j++){

				if(arr[j-1]>arr[j]){

					int temp = arr[j-1];
					arr[j-1] = arr[j];
					arr[j] = temp;
				}
			}
		}

		for(int i=0; i<arr.length; i++){

			System.out.print(arr[i] + " ");
		}
		System.out.println();

	}
}


