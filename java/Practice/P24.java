import java.io.*;

class ExceptionDemo{
	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter No. : ");

		int x = Integer.parseInt(br.readLine());

		try{
			if(x==0){
				throw new ArithmeticException("Divide by zero");
			}
			System.out.println(10/x);
		}catch(ArithmeticException ae){

			System.out.println("Exception in thread " + Thread.currentThread().getName() + " ");

			ae.printStackTrace();
		}
	}
}

			
