class Demo{

	private String str = null;

	Demo(String str){
		this.str = str;

		StringBuffer sb = new StringBuffer(str);

		str = sb.reverse().toString();

		System.out.println(str);
	}
}

class Client{

	public static void main(String[] args){

		Demo obj = new Demo("Hello");
		
	}
}



