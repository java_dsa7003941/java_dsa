abstract class A{

	abstract void abc();

	void def(){
		System.out.println("Hiiii....!");
	}
}
class B extends A{
	
	void abc(){
		System.out.println("Hello.....");
	}
}
class Client{
	public static void main(String[] args){

		B obj = new B();

		obj.abc();
		obj.def();

	}
}

