import java.util.*;

class Demo{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter starting range ");
		int start = sc.nextInt();

		System.out.println("Enter ending range ");
		int end = sc.nextInt();

		int ans = checkPallindrome(start,end);

		System.out.println(ans);
	}

	static int checkPallindrome(int start,int end){

		int count=0;
		for(int i=start; i<=end; i++){

			int temp = i;
			int num=0;

			for(int j=0; temp>0; temp = temp/10){

				int rem = temp%10;
				num = num*10 + rem;
			}
			if(num==i)
				count++;

		}
		return count;
		
	}
}


