import java.io.*;

class StringDemo{

	static void PalindromeString(String str1){

		char arr1[] = str1.toCharArray();

		int tp = arr1.length-1;
		int ct = 0;

		for(int i=0; i<arr1.length; i++){

			if(arr1[i]==arr1[tp]){

				ct++;
			}
			tp--;
		}

		if(arr1.length==ct){

			System.out.println("Palindrome String");
		}else{
		
			System.out.println("Not a Palindrome String");
		}
	}

		boolean AnagramString(String str2, String str3){

			char arr2[]= str2.toCharArray();
			char arr3[]= str3.toCharArray();

			if(arr2.length!=arr3.length){

				return false;
			}else{
				for(int i=0; i<arr2.length;i++){
					for(int j=i+1; j<arr2.length; j++){

						if(arr2[i]>arr2[j]){

							char tp1 = arr2[i];
							arr2[i] = arr2[j];
							arr2[j] = tp1;
						}
						if(arr3[i]>arr3[j]){

							char tp2 = arr3[i];
							arr3[i] = arr3[j];
							arr3[j] = tp2;
						}
					}
				}

				for(int i=0; i<arr2.length; i++){

					if(arr2[i]!=arr3[i]){
						return false;
					}
				}
			}
			return true;
		}

		public static void main(String[] args)throws IOException{

			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			
			StringDemo obj = new StringDemo();

			System.out.println("Check Palindrome String");

			System.out.println("Enter the String :");

			String str1 = br.readLine();

			PalindromeString(str1);

			System.out.println("Check Anagram String");

			System.out.println("Enter First String :");
			String str2 = br.readLine();

			System.out.println("Enter Second String ;");
			String str3 = br.readLine();

			if(obj.AnagramString(str2,str3)){

				System.out.println("String is Anagram");
			}else{
				System.out.println("Not a Anagram String");
			}

		}
	}


