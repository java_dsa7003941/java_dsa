class UnderAgeException extends RuntimeException{

	UnderAgeException(String str){

		super(str);
	}
}
class Demo{

	public static void main(String[] args){

	int age = 19;

	if(age<18){

		throw new UnderAgeException("Age less than 18");
	}

	System.out.println(age);
	
	}
}
