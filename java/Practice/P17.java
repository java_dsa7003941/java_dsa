class SingleTon{
	
	private SingleTon(){

	}
	SingleTon obj = new SingleTon();

	static SingleTon getObject(){

		return obj;

	}
}


class Demo{
	public static void main(String[] args){

		SingleTon obj1 = SingleTon.getObject();
		System.out.println(obj1);

		SingleTon obj2 = SingleTon.getObject();
                System.out.println(obj2);

		SingleTon obj3 = SingleTon.getObject();
                System.out.println(obj3);
	}
}

