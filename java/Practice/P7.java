import java.io.*;

class Demo{
	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter String : ");
		String str = br.readLine();

		StringBuffer sb = new StringBuffer(str);

		str = sb.reverse().toString();

		System.out.println(str);
	}
}

