class Demo{

	public static void main(String[] args){

		StringBuffer sb = new StringBuffer("geeksForGeeks");
		convertOpposite(sb);

		System.out.println(sb);
	}

	static void convertOpposite(StringBuffer str){
        int ln = str.length();
 
        // Conversion using predefined methods
        for (int i = 0; i < ln; i++) {

            Character c = str.charAt(i);

            if (Character.isLowerCase(c))
                str.replace(i, i + 1,Character.toUpperCase(c) + "");
            else
                str.replace(i, i + 1,Character.toLowerCase(c) + "");
        }

     }
}

