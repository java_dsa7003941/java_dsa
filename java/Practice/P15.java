class MyThread extends Thread{
	MyThread(ThreadGroup tg, String str){
		super(tg,str);
	}
	public void run(){
		System.out.println(Thread.currentThread());
	
	}
}
class ThreadDemo{
	public static void main(String[] args){

		ThreadGroup pthread = new ThreadGroup("Core2Web");

		MyThread obj1 = new MyThread(pthread, "C");
		MyThread obj2 = new MyThread(pthread, "Java");
		MyThread obj3 = new MyThread(pthread, "Python");

		obj1.start();
		obj2.start();
		obj3.start();

		ThreadGroup cthread = new ThreadGroup(pthread,  "encubator");

		System.out.println(pthread.activeGroupCount());

		MyThread obj4 = new MyThread(cthread, "Flutter");
		MyThread obj5 = new MyThread(cthread, "SpringBoot");
		MyThread obj6 = new MyThread(cthread, "ReactJS");

		obj4.start();
		obj5.start();
		obj6.start();

	}
}

