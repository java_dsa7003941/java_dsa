import java.io.*;

class PallindromeDemo{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the number :");

		int num = Integer.parseInt(br.readLine());

		int temp = num;
		int sum = 0;

		for(int i=0; temp>0; temp=temp/10){

			int rev = temp%10;

			sum = sum*10 + rev ;
		}
		if (num==sum){
			System.out.println("Number is Pallindrome");
		}else{
			System.out.println("Number is not Pallindrome");
		}
	}
}

